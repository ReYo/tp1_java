package alimentation.produit;

import alimentation.composant.Glucide;
import alimentation.composant.Lipide;
import alimentation.composant.Proteine;
import alimentation.util.Origine;

/**
 * Classe qui stock le nombre de Proteine,Lipide,Glucide et l'origine d'un aliment
 *
 * @author Yoann
 */
public class ProduitAlimentaire {

    private Proteine proteine;
    private Lipide lipide;
    private Glucide glucide;
    private Origine origine;

    /**
     * Constructeur
     * @param proteine Reference à ses proteines
     * @param lipide Reference à ses lipides
     * @param glucide Reference à ses glucides
     * @param origine Reference à son origine
     */
    public ProduitAlimentaire(Proteine proteine, Lipide lipide, Glucide glucide, Origine origine) {
        if (proteine == null) {
            throw new NullPointerException("proteine null");
        }
        if (lipide == null) {
            throw new NullPointerException("lipide null");
        }
        if (glucide == null) {
            throw new NullPointerException("glucide null");
        }
        if (origine == null) {
            throw new NullPointerException("origine null");
        }
        this.proteine = proteine;
        this.lipide = lipide;
        this.origine = origine;
        this.glucide = glucide;
    }

    /**
     * Retourne l'origne de l'objet courrant
     *
     * @return l'origine de l'objet courrant
     */
    public Origine getOrigine() {
        return origine;
    }

    public float calculerKcalProduit() {
        return this.proteine.calculerKcal() + this.lipide.calculerKcal() + this.glucide.calculerKcal();
    }

    /**
     * Affiche de manière détaillé l'objet courrant ProduitAlimentaire
     *
     * @return les détails de l'objet courrant ProduitAlimentaire
     */
    @Override
    public String toString() {
        return "ProduitAlimentaire{" +
                "proteine=" + proteine +
                ", lipide=" + lipide +
                ", glucide=" + glucide +
                ", origine=" + origine +
                '}';
    }
}
