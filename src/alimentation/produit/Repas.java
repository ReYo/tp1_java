package alimentation.produit;

import alimentation.util.Origine;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui regroupe tous les aliments d'un repas et qui permet de calculer les Kcal total.
 *
 * @author Remy
 */
public class Repas {

    private List<ProduitAlimentaire> produits;

    /**
     * Constructeur de repas
     */
    public Repas() {
        produits = new ArrayList<>();
    }

    /**
     * Procédure qui permet d'ajouter un produit au repas
     *
     * @param produits, correspond au produit à ajouter au repas
     * @param nombre,   correspond au nombre de produit à ajouter au repas
     */
    public void ajouterProduitAlimentaire(ProduitAlimentaire produits, int nombre) {
        if (produits == null) {
            throw new NullPointerException();
        }
        if (nombre <= 0) {
            throw new IllegalArgumentException();
        }

        for (int i = 0; i < nombre; i++) {
            this.produits.add(produits);
        }
    }

    /**
     * fonction qui calcul le nombre de calories totales du repas
     *
     * @return kcal, (float) le nombre de calories totales du repas
     */
    public float calculerKcalTotal() {
        float kcal = 0.0F;

        for (ProduitAlimentaire produit : produits) {
            kcal += produit.calculerKcalProduit();
        }

        return kcal;
    }

    /**
     * fonction qui calcul le nombre de calories totales des produits végéteaux du repas
     *
     * @return kcal (float), nombre de calories
     */
    public float calculerKcalOrigineVegetalTotale() {
        float kcal = 0.0F;

        for (ProduitAlimentaire produit : produits) {
            if (produit.getOrigine() == Origine.VEGETALE) {
                kcal += produit.calculerKcalProduit();
            }
        }

        return kcal;
    }

}
