package alimentation.composant;

/**
 * Classe qui définis un type d'aliment : Glucide.
 *
 * @author Remy
 */
public class Glucide extends Aliment {

    private static final float NB_KCAL_PAR_G = 9.0F;

    /**
     * Constructeur de glucide
     * @param masse, correspond à la masse de glucide
     */
    public Glucide (float masse)
    {
        super(masse);
    }

    /**
     * Retourne la constante static du nombre de Kcal par G
     *
     * @return Retourne la constante static du nombre de Kcal par G
     */
    @Override
    public float getNbKcalParG()
    {
        return Glucide.NB_KCAL_PAR_G;
    }

    /**
     * Affiche de manière détaillé l'objet courrant Glucide
     *
     * @return les détails de l'objet courrant Glucide
     */
    @Override
    public String toString()
    {
        return "Masse = " + this.getMasse() + " nbCal : "+this.getNbKcalParG();
    }
}
