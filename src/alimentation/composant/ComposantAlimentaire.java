package alimentation.composant;

/**
 * Interface d'un aliment
 *
 * @author Yoann
 * 10/11/2015
 */
public interface ComposantAlimentaire {

    float calculerKcal();

    float getNbKcalParG();
}
