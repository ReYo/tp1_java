package alimentation.composant;

import alimentation.util.MasseImpl;

/**
 * Classe qui implémente la méthode base des aliments et qui construit sa masse.
 *
 * @author Remy
 *
 */
public abstract class Aliment extends MasseImpl implements ComposantAlimentaire{

    /**
     * Constructeur d'aliment
     * @param masse, Correspond à la masse de l'aliment
     */
    public Aliment(float masse) {
        super(masse);
    }

    /**
     * Calculer les Kcal d'un aliment
     * @return les Kcal d'un aliement
     */
    @Override
    public float calculerKcal() {
        return super.getMasse() * this.getNbKcalParG();
    }
}
