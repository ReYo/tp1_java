package alimentation.composant;

/**
 * Classe qui définis un type d'aliment : Lipide.
 *
 * @author Remy
 */
public class Lipide extends Aliment {

    private static final float NB_KCAL_PAR_G = 4.0F;

    /**
     * Constructeur de lipide
     * @param masse, correspond à la masse de lipide
     */
    public Lipide (float masse)
    {
        super(masse);
    }


    /**
     * Retourne la constante static du nombre de Kcal par G
     *
     * @return Retourne la constante static du nombre de Kcal par G
     */
    @Override
    public float getNbKcalParG()
    {
        return Lipide.NB_KCAL_PAR_G;
    }


    /**
     * Affiche de manière détaillé l'objet courrant Lipide
     *
     * @return les détails de l'objet courrant Lipide
     */
    @Override
    public String toString()
    {
        return "Masse = " + this.getMasse() + " nbCal : "+this.getNbKcalParG();
    }
}
