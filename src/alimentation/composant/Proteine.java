package alimentation.composant;

/**
 * Classe qui définis un type d'aliment : Proteine.
 *
 * @author Remy
 */
public class Proteine extends Aliment {

    private static final float NB_KCAL_PAR_G = 4.0F;

    /**
     * Constructeur de protéine
     * @param masse, correspond à la masse de protéine
     */
    public Proteine (float masse)
    {
        super(masse);
    }


    /**
     * Retourne la constante static du nombre de Kcal par G
     *
     * @return Retourne la constante static du nombre de Kcal par G
     */
    @Override
    public float getNbKcalParG()
    {
        return Proteine.NB_KCAL_PAR_G;
    }


    /**
     * Affiche de manière détaillé l'objet courrant Proteine
     *
     * @return les détails de l'objet courrant Proteine
     */
    @Override
    public String toString()
    {
        return "Masse = " + this.getMasse() + " nbCal : "+this.getNbKcalParG();
    }
}
