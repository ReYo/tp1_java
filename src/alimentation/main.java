package alimentation;

import alimentation.composant.Glucide;
import alimentation.composant.Lipide;
import alimentation.composant.Proteine;
import alimentation.produit.ProduitAlimentaire;
import alimentation.produit.Repas;
import alimentation.util.Origine;

/**
 * Classe de test du package alimentation
 *
 * @author Yoann
 * on 10/11/2015.
 */
public class Main {

    static Repas repas = new Repas();

    /**
     * Point d'entré du programme
     *
     * @param args Arguments de la ligne de commande
     */
    public static void main(String args[]) {
        repas.ajouterProduitAlimentaire(new ProduitAlimentaire(new Proteine(27.0F), new Lipide(8.0F), new Glucide(0.0F), Origine.ANIMALE),3);
        repas.ajouterProduitAlimentaire(new ProduitAlimentaire(new Proteine(6.3F), new Lipide(4.6F), new Glucide(0.9F), Origine.ANIMALE),6);
        repas.ajouterProduitAlimentaire(new ProduitAlimentaire(new Proteine(0.8F), new Lipide(0.0F), new Glucide(8.0F),Origine.VEGETALE),10);
        repas.ajouterProduitAlimentaire(new ProduitAlimentaire(new Proteine(4.0F), new Lipide(1.0F), new Glucide(1.0F),Origine.VEGETALE),3);
        repas.ajouterProduitAlimentaire(new ProduitAlimentaire(new Proteine(6.3F), new Lipide(0.2F), new Glucide(9.6F),Origine.ANIMALE),5);
        repas.ajouterProduitAlimentaire(new ProduitAlimentaire(new Proteine(14.0F), new Lipide(4.4F), new Glucide(154.0F),Origine.VEGETALE),5);
        System.out.println(repas.calculerKcalTotal());
        System.out.println(repas.calculerKcalOrigineVegetalTotale());
    }
}
