package alimentation.util;

/**
 * Interface masse.
 *
 * @author Yoann
 * on 10/11/2015.
 */
public interface Masse {

    float getMasse();

}
