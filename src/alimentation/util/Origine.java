package alimentation.util;

/**
 * Enumération des origines possibles d'un aliment.
 *
 * @author Yoann
 * on 10/11/2015.
 */
public enum Origine {
    VEGETALE, ANIMALE
}
