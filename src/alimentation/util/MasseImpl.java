package alimentation.util;

/**
 * Implémentation de l'interface Masse qui stock un flottant.
 *
 * @author Yoann
 * on 10/11/2015.
 */
public class MasseImpl implements Masse{

    private float masse;

    /**
     * Constructeur de la classe MasseImpl, qui vérifie que la masse soit >= 0.
     *
     * @param masse Valeur de la masse.
     */
    public MasseImpl(float masse) {
        if (masse < 0.0F) {
            throw new IllegalArgumentException("La masse ne peut être inférieur à 0");
        }
        this.masse = masse;
    }

    /**
     * Retourne la masse stocké par l'objet.
     *
     * @return valeur de la masse de l'objet courrant
     */
    @Override
    public float getMasse() {
        return masse;
    }
}
